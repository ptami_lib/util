module gitlab.com/ptami_lib/util

go 1.14

require (
	github.com/aws/aws-sdk-go v1.40.39
	github.com/aws/aws-sdk-go-v2 v1.16.3
	github.com/aws/aws-sdk-go-v2/config v1.15.4
	github.com/aws/aws-sdk-go-v2/service/s3 v1.26.6
	github.com/jinzhu/gorm v1.9.16
	github.com/oklog/ulid/v2 v2.0.2
	github.com/sendgrid/rest v2.6.4+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.10.0+incompatible
	github.com/stretchr/testify v1.7.0 // indirect
)
